#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# File: /Users/paolopierini/Nextcloud/Development/BWexplorer/bwGraphlib.py
# Project: /Users/paolopierini/Nextcloud/Development/BWexplorer
# Created Date: Tuesday, July 27th 2021, 5:17:59 pm
# Author: Paolo Pierini
# -----
# Last Modified: Mon May 16 2022
# Modified By: Paolo Pierini
# -----
# Copyright (c) 2021 European Spallation Source Eric
# 
# Use it at your risk!
# It's free software anyway
# -----
# HISTORY:
# Date      	By	Comments
# ----------	---	----------------------------------------------------------
###
#from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QVBoxLayout
from matplotlib.backends.qt_compat import QtCore, QtWidgets, QtGui
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.path as mpath
from matplotlib.pyplot import tight_layout

'''
srfCanvas
=========
'''
class srfCanvas(FigureCanvas):
    """
    This is the container for the configuration of matplotlib.
    We can then import and use it in the applicaitons and implement only the specific plot function.
    All the rest is defined here.
    Applocations should subclass it and overload the plot function.
    """
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        #Creates the figure object
        self.pars={'width':width,'height':height,'dpi':dpi, 
                   'left': 0.14, 'right': 0.86, 'bottom': 0.12, 'top': 0.9}
        self.figure = Figure(figsize=(self.pars['width'], 
                                      self.pars['height']), 
                                      dpi=self.pars['dpi'])
        self.axes = self.figure.subplots() # Adds plot region
        self.figure.subplotpars.left=self.pars['left']
        self.figure.subplotpars.right=self.pars['right']
        self.figure.subplotpars.bottom=self.pars['bottom']
        self.figure.subplotpars.top=self.pars['top']
        #Initializes the default FigureCanvas superclass
        super().__init__(self.figure)
        self.setParent(parent) # on None
        self.form=None

        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
                           QtWidgets.QSizePolicy.Expanding)
        self.updateGeometry()
        self.axes.set_visible(False) 

    def prepare(self):
        self.figure.clear()
        self.axes=self.figure.subplots()
        self.figure.subplotpars.left=self.pars['left']
        self.figure.subplotpars.right=self.pars['right']
        self.figure.subplotpars.bottom=self.pars['bottom']
        self.figure.subplotpars.top=self.pars['top']
     
    def erase(self):
        self.figure.clear()
        self.draw()
        
    def hideme(self,b):
        self.axes.set_visible(b)
        self.draw()

    #def plot(self,**kwargs):
    #def save(self,**kargs):

class srfWidget(QtWidgets.QWidget):
    def __init__(self, parent = None):
        QtWidgets.QWidget.__init__(self,parent)
        self.canvas= srfCanvas()
        self.vbl=QVBoxLayout()
        self.vbl.addWidget(self.canvas)
        self.setLayout(self.vbl)

# The main window class
"""
Main application Window
=======================
"""
class srfMainWindow(QtWidgets.QMainWindow):
    '''
    Needs a title and the definition of the central widget to show in the middle.
    '''
    def __init__(self, parent = None, title='', centralWidget=None):
        super().__init__(parent)
        self.title=title
        # it contains the central widget
        self.setCentralWidget(centralWidget)
        self.initializeGUI()

    """ Initialize """
    def initializeGUI(self):
        QtWidgets.QToolTip.setFont(QtGui.QFont('SansSerif', 14))
        self.setToolTip('***My Tooltip***')
        #Menu
        mnuExit=QtWidgets.QAction('&End Program',self)
        mnuExit.setStatusTip('Exit')
        mnuExit.triggered.connect(self.close)
        mnuSBar=QtWidgets.QAction('&View Statusbar',self,checkable=True)
        mnuSBar.setChecked(True)
        mnuSBar.triggered.connect(self.toggleSBView)
        menu=self.menuBar()
        #menu.setNativeMenuBar(False)   # menubar is inside the window left corner
        mnufile=menu.addMenu('&Options')
        mnufile.addAction(mnuExit)
        mnufile.addAction(mnuSBar)
        self.statusBarView()
        self.resize(900,800)
        #self.center()
        self.setWindowTitle(self.title)
        self.show()
    """ StatusBar """
    def statusBarView(self):
        pass
    """ Menu """
    def toggleSBView(self,state):
        if state:
            self.statusBar().show()
        else:
            self.statusBar().hide()
    """ Context menu """            
    def contextMenuEvent(self, event):     
        cmenu = QtWidgets.QMenu(self)
        quitAct = cmenu.addAction("Quit")
        # to map the coordinates so that 
        action = cmenu.exec_(self.mapToGlobal(event.pos()))
        if action == quitAct:
            self.close()
    def center(self):
        fg=self.frameGeometry()
        cp = QtWidgets.QDesktopWidget().availableGeometry().center()
        fg.moveCenter(cp)
        self.move(fg.topLeft())
    """ Traps the close Event """
    def closeEvent(self,event):
        reply = QtWidgets.QMessageBox.question(self,'Hej!','Are you sure to quit?',
                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No)
        if reply == QtWidgets.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()
