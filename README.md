# SetTS2Tables

Set TS2 SP/FF tables. This is a toy PyQt project for muyuan and me to learn

## Scope
Develop a tool for setting FF and SP tables as we want them for the LLRF operation, independently of the tools developed by LLRF.

Understand if the PyQt environment on the consoles is a viable alternative to SNL IOCs for high level applications

## Background

Rely on Arek and Benjamin work on the setting of a conda pyqt env.

## Authors and acknowledgment
Paolo and Muyan

