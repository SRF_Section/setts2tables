#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# File: /Users/paolopierini/Nextcloud/Development/BWexplorer/Template.py
# Project: /Users/paolopierini/Nextcloud/Development/BWexplorer
# Created Date: Tuesday, November 10th 2020, 11:42:58 am
# Author: Paolo Pierini
# -----
# Last Modified: Mon May 16 2022
# Modified By: Paolo Pierini
# -----
# Copyright (c) 2020 European Spallation Source Eric
# 
# Use it at your risk!
# It's free software anyway
# -----
# HISTORY:
# Date      	By	Comments
# ----------	---	----------------------------------------------------------
###
import sys
import os
import time
from matplotlib import rcParams
import numpy as np
import epics
#import math
#import PyQt5

from matplotlib.backends.qt_compat import QtCore, QtWidgets, QtGui
from matplotlib.backends.backend_qt5agg import FigureCanvas, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.path as mpath

#from termcolor import cprint, colored
from srfGraphlib import srfMainWindow, srfCanvas

import warnings
warnings.filterwarnings('ignore')

debug=True
Simulation=False
version={'maj':1,'min':2,'auth':'PP','when':'2022-05-16'}

def get_Ql(x,y, end, min,max):
    fillvalue=y[np.where(x>end)][0]
    decay_cond=(x>end)&(y<=max*fillvalue)&(y>=min*fillvalue)
    decay=np.array([x[decay_cond],y[decay_cond]])
    decaylog=np.array([decay[0],np.log(decay[1])])
    newseries=np.polynomial.polynomial.Polynomial.fit(decaylog[0]-end,decaylog[1],1)
    cof=newseries.convert().coef
    # Tau from Eacc
    tau=-1./cof[1]*1000
    Ql=np.pi*704.420*tau
    return decay,tau,Ql

'''
If there is a graph it goes here
'''
class pltDecay(srfCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        super().__init__(parent=None, width=5, height=4, dpi=100)
        self.axes=self.figure.subplots()
        self.figure.subplotpars.left=self.pars['left']
        self.figure.subplotpars.right=self.pars['right']
        self.figure.subplotpars.bottom=self.pars['bottom']
        self.figure.subplotpars.top=self.pars['top']

    #Plotting function
    def plot(self,dic):
        self.figure.clear()
        self.axes=self.figure.subplots()
        self.figure.subplotpars.left=self.pars['left']
        self.figure.subplotpars.right=self.pars['right']
        self.figure.subplotpars.bottom=self.pars['bottom']
        self.figure.subplotpars.top=self.pars['top']
        #self.prepare()
        if dic:
            if 'spm' in dic.keys():
                self.axes.plot(dic['x'][:len(dic['spm'])],dic['spm'],'g-',lw=3)
            self.axes.plot(dic['xfit'],dic['yfit'],'r-',lw=4)
            self.axes.plot([dic['xfit'][0],dic['xfit'][0]],[0.0,dic['yfit'][0]],'k:')
            self.axes.plot([dic['xfit'][-1],dic['xfit'][-1]],[0.0,dic['yfit'][-1]],'k:')
            self.axes.plot(dic['x'],dic['y'],'b-',lw=1)
            self.axes.set_xlabel(dic['xlabel'])
            self.axes.set_ylabel(dic['ylabel'])
            self.axes.grid()
            self.figure.set_constrained_layout(True)
            self.draw()

class pltTable(srfCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        super().__init__(parent=None, width=5, height=4, dpi=100)
    #Plotting function
    def plot(self,dic):
        self.prepare()
        self.axes.plot(dic['x'],dic['Mag'],'b-')
        self.axes.tick_params(axis='y', colors='b')
        ax2=self.axes.twinx()
        ax2.plot(dic['x'],dic['Pha'],'r-')
        ax2.tick_params(axis='y', colors='r')
        self.axes.set_xlabel(dic['xlabel'])
        self.axes.set_ylabel(dic['Mlabel'],color='b')
        ax2.set_ylabel(dic['Plabel'],color='r')
        self.axes.grid()
        self.figure.set_constrained_layout(True)
        self.draw()


class myWidget(QtWidgets.QWidget):
    def __init__(self,parent):
        super().__init__()
        self.Config={}
        self.parent=parent
        #
        self.timer=QtCore.QTimer()
        self.timer.timeout.connect(self.Acquire)
        self.updateTimer=QtCore.QTimer()
        self.updateTimer.timeout.connect(self.LLRFStatus)
        #
        self.__controls()
        self.__layout()
        self.__bindings()

    def vstring(self):
        name=os.path.basename(sys.modules['__main__'].__file__).split(".")[0]
        return '{:s}, V.{:d}.{:d}, by {:s} on {:s}'.format(name,version['maj'],version['min'],version['auth'],version['when'])

    def __controls(self):
        self.lblVersion=QtWidgets.QLabel(self.vstring(),self)
        # Settings
        self.grpSettings=QtWidgets.QGroupBox("Choose Cavity",self)
        self.syslabel=QtWidgets.QLabel("System", self)
        self.sys=QtWidgets.QLabel("TS2-010RFC:", self)
        self.dig=[QtWidgets.QLabel("201",self),QtWidgets.QLabel("202",self),QtWidgets.QLabel("101",self),QtWidgets.QLabel("102",self)]
        self.rdCav=[QtWidgets.QRadioButton("CAV1",self),QtWidgets.QRadioButton("CAV2",self),QtWidgets.QRadioButton("CAV3",self),QtWidgets.QRadioButton("CAV4",self)]
        for rd in self.rdCav:
            rd.toggled.connect(self.onCavityChange)
        self.lblDprefix=QtWidgets.QLabel("Please Select a Cavity",self)
        self.lblDprefix.setStyleSheet("QLabel {background-color :blue; color: white}")
        self.lblLprefix=QtWidgets.QLabel("Please Select a Cavity",self)
        self.lblLprefix.setStyleSheet("QLabel {background-color :blue; color: white}")
        # LLRF Status
        self.grpLLRFstat=QtWidgets.QGroupBox("LLRF",self)
        self.lblStatus=QtWidgets.QLabel("Status:",self)
        self.Status=QtWidgets.QLabel("N/A",self)
        self.lblMain=QtWidgets.QLabel("is main: ",self)
        self.Main=QtWidgets.QLabel("",self)
        self.Main.setFixedSize(16,16)
        self.lblLoop=QtWidgets.QLabel("Loop Mode:", self)
        self.Loop=QtWidgets.QLabel("",self)
        self.lblF0=QtWidgets.QLabel("Frequency:",self)
        self.F0=QtWidgets.QLabel("N/A",self)
        self.lblFS=QtWidgets.QLabel("Sampling Frequency:",self)
        self.FS=QtWidgets.QLabel("N/A",self)
        self.lblIQN=QtWidgets.QLabel("IQN:",self)
        self.IQN=QtWidgets.QLabel("N/A",self)
        self.lbldt=QtWidgets.QLabel('Timestep:',self)
        self.dt=QtWidgets.QLabel('N/A',self)
        # Timing 
        self.grpTim=QtWidgets.QGroupBox("Timing",self)
        self.lblRFWidth=QtWidgets.QLabel("RF Pulse width [us]:")
        self.RFW=QtWidgets.QLabel("N/A",self)
        self.lblRepRate=QtWidgets.QLabel("Repetition Rate [Hz]:")
        self.RR=QtWidgets.QLabel("N/A",self)
        self.lblTimestamp=QtWidgets.QLabel("Timestamp:")
        self.Timestamp=QtWidgets.QLabel('N/A')
        # Calibration
        self.grpCalibration=QtWidgets.QGroupBox("Calibration",self)
        self.btnAcquire=QtWidgets.QPushButton("Acquire PU",self)
        self.btnAcquire.clicked.connect(self.Acquire)
        self.btnStartAcq=QtWidgets.QPushButton('Start 1s refresh', self)
        self.btnStartAcq.clicked.connect(self.StartTimer)
        self.btnStopAcq=QtWidgets.QPushButton('Stop 1s refresh', self)
        self.btnStopAcq.clicked.connect(self.StopTimer)
        self.pltDecay=pltDecay(self,width=7,height=6)
        self.qllabel=QtWidgets.QLabel("QL=",self)
        self.ql=QtWidgets.QLabel("{:.1f}".format(0),self)
        self.tplabel=QtWidgets.QLabel("tau (power)=",self)
        self.tp=QtWidgets.QLabel("{:.1f} [us]".format(0),self)
        self.tflabel=QtWidgets.QLabel("tau (field)=",self)
        self.tf=QtWidgets.QLabel("{:.1f} [us]".format(0),self)
        # Tables
        self.grpTables=QtWidgets.QGroupBox("Tables",self)
        self.lblSP=QtWidgets.QLabel('Set Point Table',self)
        self.pltSP=pltTable(self,width=7,height=6)
        self.lblFF=QtWidgets.QLabel('Feed Forward Table',self)
        self.pltFF=pltTable(self,width=7,height=6)
        self.Eacclabel=QtWidgets.QLabel("Eacc [MV/m]=",self)
        self.Eacc=QtWidgets.QLineEdit("N/A",self)
        self.Eacc.setMinimumWidth(50)
        self.tfilllabel=QtWidgets.QLabel("tfill [us]=",self)
        self.tfill=QtWidgets.QLineEdit("N/A",self)
        self.tflatlabel=QtWidgets.QLabel("tflat [us]=",self)
        self.tflat=QtWidgets.QLineEdit("N/A",self)
        self.lblroverq=QtWidgets.QLabel("R/Q [Ohm]=",self)
        self.roverq=QtWidgets.QLineEdit("374.0",self)
        self.lblL=QtWidgets.QLabel("Lcav [m]=",self)
        self.L=QtWidgets.QLineEdit("0.855",self)
        self.btnGenerateTables=QtWidgets.QPushButton("Generate Tables",self)
        self.btnGenerateTables.clicked.connect(self.GenerateTables)
        self.btnCommitTables=QtWidgets.QPushButton("Commit Tables",self)
        self.btnCommitTables.clicked.connect(self.CommitTables)
        self.lblComment1=QtWidgets.QLabel("",self)
        self.lblComment2=QtWidgets.QLabel("",self)
        self.lblComment3=QtWidgets.QLabel("",self)

    def __layout(self):
        # Layout of the Settings group
        grdsetting=QtWidgets.QGridLayout()
        grdsetting.addWidget(self.syslabel, 0,0)
        grdsetting.addWidget(self.sys,0,1,1,3,QtCore.Qt.AlignCenter)
        grdsetting.addWidget(QtWidgets.QLabel('Cavity'),1,0)
        grdsetting.addWidget(QtWidgets.QLabel('Digitizer'),1,1)
        grdsetting.addWidget(QtWidgets.QLabel('Selected cavity PV prefixes'),1,2)
        grdsetting.addWidget(self.rdCav[0]    ,2,0)
        grdsetting.addWidget(self.dig[0]      ,2,1)
        grdsetting.addWidget(self.rdCav[1]    ,3,0)
        grdsetting.addWidget(self.dig[1]      ,3,1)
        grdsetting.addWidget(self.rdCav[2]    ,4,0)
        grdsetting.addWidget(self.dig[2]      ,4,1)
        grdsetting.addWidget(self.rdCav[3]    ,5,0)
        grdsetting.addWidget(self.dig[3]      ,5,1)
        grdsetting.addWidget(self.lblDprefix  ,2,2,2,1,QtCore.Qt.AlignVCenter)
        grdsetting.addWidget(self.lblLprefix  ,4,2,2,1,QtCore.Qt.AlignVCenter)
        self.grpSettings.setLayout(grdsetting)
        # Layout of the LLRF group
        grdsetting=QtWidgets.QGridLayout()
        grdsetting.addWidget(self.lblStatus,0,0,QtCore.Qt.AlignRight)
        grdsetting.addWidget(self.Status,0,1,QtCore.Qt.AlignCenter)
        grdsetting.addWidget(self.lblMain,1,0,QtCore.Qt.AlignRight)
        grdsetting.addWidget(self.Main,1,1,QtCore.Qt.AlignCenter)
        grdsetting.addWidget(self.lblLoop,2,0,QtCore.Qt.AlignRight)
        grdsetting.addWidget(self.Loop,2,1,QtCore.Qt.AlignCenter)
        grdsetting.addWidget(self.lblF0,3,0,QtCore.Qt.AlignRight)
        grdsetting.addWidget(self.F0,3,1)
        grdsetting.addWidget(self.lblFS,4,0,QtCore.Qt.AlignRight)
        grdsetting.addWidget(self.FS,4,1)
        grdsetting.addWidget(self.lblIQN,5,0,QtCore.Qt.AlignRight)
        grdsetting.addWidget(self.IQN,5,1)
        grdsetting.addWidget(self.lbldt,6,0,QtCore.Qt.AlignRight)
        grdsetting.addWidget(self.dt,6,1)
        self.grpLLRFstat.setLayout(grdsetting)
        # Layout of the Timing group
        grdsetting=QtWidgets.QGridLayout()
        grdsetting.addWidget(self.lblRFWidth,0,0,QtCore.Qt.AlignRight)
        grdsetting.addWidget(self.RFW,0,1)
        grdsetting.addWidget(self.lblRepRate,1,0,QtCore.Qt.AlignRight)
        grdsetting.addWidget(self.RR,1,1)
        grdsetting.addWidget(self.lblTimestamp,2,0,QtCore.Qt.AlignRight)
        grdsetting.addWidget(self.Timestamp,2,1)
        self.grpTim.setLayout(grdsetting)
        # Layout of the Calibration group
        horsetting=QtWidgets.QHBoxLayout()
        versetting=QtWidgets.QVBoxLayout()
        versetting.addStretch(1)
        versetting.addWidget(self.btnAcquire)
        versetting.addWidget(self.btnStartAcq)
        versetting.addWidget(self.btnStopAcq)
        versetting.addStretch(1)
        horsetting.addLayout(versetting)
        horsetting.addWidget(self.pltDecay)
        grdsetting=QtWidgets.QGridLayout()
        grdsetting.addWidget(self.qllabel,0,0)
        grdsetting.addWidget(self.ql,0,1)
        grdsetting.addWidget(self.tplabel,1,0)
        grdsetting.addWidget(self.tp,1,1)
        grdsetting.addWidget(self.tflabel,2,0)
        grdsetting.addWidget(self.tf,2,1)
        versetting=QtWidgets.QVBoxLayout()
        versetting.addLayout(grdsetting)
        versetting.addStretch(1)
        horsetting.addLayout(versetting)
        horsetting.addStretch(1)
        self.grpCalibration.setLayout(horsetting)
        # Layout of the Tables group
        grdsetting=QtWidgets.QGridLayout()
        grdsetting.addWidget(self.Eacclabel,0,0)
        grdsetting.addWidget(self.Eacc,0,1)
        grdsetting.addWidget(self.tfilllabel,1,0)
        grdsetting.addWidget(self.tfill,1,1)
        grdsetting.addWidget(self.tflatlabel,2,0)
        grdsetting.addWidget(self.tflat,2,1)
        grdsetting.addWidget(self.lblroverq,3,0)
        grdsetting.addWidget(self.roverq,3,1)
        grdsetting.addWidget(self.lblL,4,0)
        grdsetting.addWidget(self.L,4,1)
        grdsetting.addWidget(self.btnGenerateTables,5,0,1,2)
        grdsetting.addWidget(self.btnCommitTables,6,0,1,2)
        grdsetting.addWidget(self.lblComment1,7,0,1,2)
        grdsetting.addWidget(self.lblComment2,8,0,1,2)
        grdsetting.addWidget(self.lblComment3,9,0,1,2)
        versetting=QtWidgets.QVBoxLayout()
        versetting.addLayout(grdsetting)
        versetting.addStretch(1)
        horsetting=QtWidgets.QHBoxLayout()
        horsetting.addLayout(versetting)
        versetting=QtWidgets.QVBoxLayout()
        versetting.addWidget(self.lblSP)
        versetting.addWidget(self.pltSP)
        horsetting.addLayout(versetting)
        versetting=QtWidgets.QVBoxLayout()
        versetting.addWidget(self.lblFF)
        versetting.addWidget(self.pltFF)
        horsetting.addLayout(versetting)
        self.grpTables.setLayout(horsetting)
        # First line
        firstline=QtWidgets.QHBoxLayout()
        ver=QtWidgets.QVBoxLayout()
        ver.addWidget(self.grpSettings)
        ver.addStretch(1)
        firstline.addLayout(ver)
        firstline.addStretch(1)
        ver=QtWidgets.QVBoxLayout()
        ver.addWidget(self.grpLLRFstat)
        ver.addStretch(1)
        firstline.addLayout(ver)
        firstline.addStretch(1)
        ver=QtWidgets.QVBoxLayout()
        ver.addWidget(self.grpTim)
        ver.addStretch(1)
        ver.addWidget(self.lblVersion)
        ver.addStretch(1)
        firstline.addLayout(ver)
        # Main layout is vertical
        mainv=QtWidgets.QVBoxLayout()
        mainv.addLayout(firstline)
        mainv.addWidget(self.grpCalibration)
        mainv.addWidget(self.grpTables)
        mainv.addStretch(1)
        self.setLayout(mainv)

    def __bindings(self):
        self.rdCav[0].setChecked(True)
        self.btnStopAcq.setEnabled(False)
        self.Eacc.setText('4.0')
        self.tfill.setText('300.0')
        self.tflat.setText('500.0')
        self.roverq.setText('374.0')
        self.L.setText('0.855')

    def StartTimer(self):
        self.timer.start(1000)
        self.btnStartAcq.setEnabled(False)
        self.btnStopAcq.setEnabled(True)

    def StopTimer(self):
        self.timer.stop()
        self.btnStartAcq.setEnabled(True)
        self.btnStopAcq.setEnabled(False)

    def LLRFStatus(self):
        # Define correct prefixes
        for i in range(len(self.rdCav)):
            if self.rdCav[i].isChecked():
                dig=self.dig[i].text()
                self.Config['PVDprefix']=self.sys.text()+"RFS-DIG-"+dig+":"
                self.Config['PVLprefix']=self.sys.text()+"RFS-LLRF-"+dig+":"
        # Fill config
        self.lblDprefix.setText(self.Config['PVDprefix'])
        self.lblLprefix.setText(self.Config['PVLprefix'])
        self.Config['F0']=epics.caget(self.Config['PVLprefix']+"FreqSystem")
        self.Config['FSampl']=epics.caget(self.Config['PVLprefix']+"FreqSampling")
        self.Config['Iqn']=epics.caget(self.Config['PVDprefix']+"IQSmpNearIQ-N-RB")
        self.Config['Status']=epics.caget(self.Config['PVDprefix']+'FSM',as_string=True)
        self.Config['isMain']=epics.caget(self.Config['PVDprefix']+'Main-RB')
        self.Config['Loop']=epics.caget(self.Config['PVDprefix']+'OpenLoop-RB')
        self.Config['dt']=1/(self.Config['FSampl']/self.Config['Iqn'])
        self.grpLLRFstat.setTitle("LLRF-"+dig)
        # Modify indicators
        self.grpCalibration.setEnabled(self.Config['Status'] == 'ON')
        # Fill info
        self.Status.setText(self.Config['Status'])
        if self.Config['Status'] == 'ON':
            self.Status.setStyleSheet('QLabel {background-color: green; color: white; border: 1px solid black}')
        else:
            self.Status.setStyleSheet('QLabel {background-color: yellow; color: red; border: 1px solid black}')
        if self.Config['isMain']:
            self.Main.setStyleSheet('QLabel {background-color: green; border: 1px solid black; border-radius: 8px;}')
        else:
            self.Main.setStyleSheet('QLabel {background-color: grey; border: 1px solid black; border-radius: 8px;}')
        if self.Config['Loop']:
            self.Loop.setText("Open")
            self.Loop.setStyleSheet('QLabel {background-color: yellow; color: red; border: 1px solid black}')
        else:
            self.Loop.setText("Closed")
            self.Loop.setStyleSheet('QLabel {background-color: green; color: white; border: 1px solid black}')
        # Fills values
        self.F0.setText('{:.3f} MHz'.format(self.Config['F0']))
        self.FS.setText('{:.3f} MHz'.format(self.Config['FSampl']))
        self.IQN.setText('{:.3f}'.format(self.Config['Iqn']))
        self.dt.setText('{:.3f} us'.format(self.Config['dt']))
        self.Config['RFW']=epics.caget('TS2-010:Ctrl-EVM-101:RFWidth-RB')
        self.Config['RR']=epics.caget('TS2-010:Ctrl-EVM-101:Mxc0-Frequency-RB')
        self.RFW.setText('{:.3f}'.format(self.Config['RFW']))
        self.RR.setText('{:.1f}'.format(self.Config['RR']))
        self.Timestamp.setText(epics.caget('TS2-010:Ctrl-EVM-101:Timestamp-RB',as_string=True))

    def onCavityChange(self):
        if hasattr(self,'updateTimer'):
            self.updateTimer.start(1000)
        self.LLRFStatus()
        self.grpTables.setEnabled(self.Config['Status'] == 'ON')
        # delete acquired trace
        self.pltDecay.erase()
        self.StopTimer()
        self.grpTables.setEnabled(False)
        self.Config['Ql']=0.0
        self.Config['tp']=0.0
        self.Config['tf']=0.0
        self.ql.setText('{:.3e}'.format(self.Config['Ql']))
        self.tp.setText('{:.1f} [us]'.format(self.Config['tp']))
        self.tf.setText('{:.1f} [us]'.format(self.Config['tf']))
        self.Config['tablesOK']=False
        self.lblComment1.setText("")
        self.lblComment2.setText("")
        self.lblComment3.setText("")

    def Acquire(self):
        time=epics.caget(self.Config['PVDprefix']+"Dwn0-XAxis")
        pu=epics.caget(self.Config['PVDprefix']+"Dwn0-Cmp0")
        rfw=self.Config['RFW']/1000
        if Simulation:
            for i in range(len(pu)):
                if time[i]<= rfw:
                    pu[i]=pu[i]+10.0
                else:
                    pu[i]=10*np.exp(-(time[i]-rfw)/0.326)
        decay,tau,ql = get_Ql(time,pu,rfw,0.5,0.9)
        setpm=epics.caget(self.Config['PVDprefix']+"SPTbl-Mag-RB")
        setpp=epics.caget(self.Config['PVDprefix']+"SPTbl-Ang-RB")
        dic={'x':time,'y':pu,
             'xlabel':'time [ms]','ylabel':'Field [MV/m]',
             'xfit': decay[0],'yfit':decay[1],
             'spm':setpm,'spp':setpp}
         
        self.pltDecay.plot(dic)
        self.Config['tf']=tau
        self.Config['tp']=tau/2
        self.Config['Ql']=ql
        self.ql.setText('{:.3e}'.format(self.Config['Ql']))
        self.tp.setText('{:.1f} [us]'.format(self.Config['tp']))
        self.tf.setText('{:.1f} [us]'.format(self.Config['tf']))
        self.grpTables.setEnabled(True)
        self.btnCommitTables.setEnabled(self.Config['tablesOK'])

    def GenerateTables(self):
        SP_Mag=[]
        SP_Pha=[]
        FF_Mag=[]
        FF_Pha=[]
        time=[]
        # get the parameters
        E0=float(self.Eacc.text())
        roverq=float(self.roverq.text())
        L=float(self.L.text())
        tfill=float(self.tfill.text())
        tflat=float(self.tflat.text())
        QL=self.Config['Ql']
        tauf=self.Config['tf']
        dt=self.Config['dt']
        ltot = int((tfill+tflat)/dt)
        E_fill=E0/(1-np.exp(-tfill/tauf))
        P_flat=np.power(E0*L,2)/(4.0*roverq*QL)*1.0E9
        P_fill=np.power(E_fill*L,2)/(4.0*roverq*QL)*1.0E9
        ratio=P_fill/P_flat
        for i in range(ltot):
            t=i*dt
            time.append(t/1000)
            if t <= 1.0:
                FFval=t*P_fill
                SPval=E_fill*(1-np.exp(-t/tauf))
            elif t <= tfill:
                FFval=P_fill
                SPval=E_fill*(1-np.exp(-t/tauf))
            else:
                FFval=P_flat
                SPval=E0
            FF_Mag.append(FFval)
            FF_Pha.append(0)
            SP_Mag.append(SPval)
            SP_Pha.append(0)

        if FF_Mag and SP_Mag and FF_Pha and SP_Pha and self.Config['isMain']:
            self.Config['tablesOK']=True
            self.Config['FF_Mag']=FF_Mag
            self.Config['FF_Pha']=FF_Pha
            self.Config['SP_Mag']=SP_Mag
            self.Config['SP_Pha']=SP_Pha
        else:
            self.Config['tablesOK']=False
        self.btnCommitTables.setEnabled(self.Config['tablesOK'])

        # do a sanity check
        if FF_Mag[-1] >= 300.0:
            self.btnCommitTables.setStyleSheet('background-color: red; color: white')
            self.btnCommitTables.setEnabled(False)
        else:
            self.btnCommitTables.setStyleSheet('')

        dicFF={'x':time,'Mag':FF_Mag,'Pha':FF_Pha,
               'xlabel': 'time [ms]',
               'Mlabel': 'Power [kW]',
               'Plabel': 'Phase [deg]'}
        self.pltFF.plot(dicFF)
        dicSP={'x':time,'Mag':SP_Mag,'Pha':SP_Pha,
               'xlabel': 'time [ms]',
               'Mlabel': 'Eacc[MV/m]',
               'Plabel': 'Phase [deg]'}
        self.pltSP.plot(dicSP)

        self.lblComment1.setText("Power {:.1f}/{:.1f} kW".format(P_fill,P_flat))
        self.lblComment2.setText("Power ratio = {:.1f}".format(ratio))

    def CommitTables(self):
        # disable fixed values
        epics.caput(self.Config['PVDprefix']+"RFCtrlCnstFFEn",0)
        epics.caput(self.Config['PVDprefix']+"RFCtrlCnstSPEn",0)
        # Forces hold-last
        epics.caput(self.Config['PVDprefix']+"FFTbl-Mode",0)
        epics.caput(self.Config['PVDprefix']+"SPTbl-Mode",0)
        # Pushes the tables to IOC
        epics.caput(self.Config['PVDprefix']+"FFTbl-Mag",self.Config['FF_Mag'])
        epics.caput(self.Config['PVDprefix']+"FFTbl-Ang",self.Config['FF_Pha'])
        epics.caput(self.Config['PVDprefix']+"SPTbl-Mag",self.Config['SP_Mag'])
        epics.caput(self.Config['PVDprefix']+"SPTbl-Ang",self.Config['SP_Pha'])
        # Commits the tables
        epics.caput(self.Config['PVDprefix']+"FFTbl-TblToFW",1)
        epics.caput(self.Config['PVDprefix']+"SPTbl-TblToFW",1)
        # Triggers the readback
        time.sleep(1.0)
        epics.caput(self.Config['PVDprefix']+"SPTbl-Mag-RB.PROC",1)
        epics.caput(self.Config['PVDprefix']+"SPTbl-Ang-RB.PROC",1)


class myMainWindow(srfMainWindow):
    def __init__(self, parent = None):
        widget=myWidget(self)
        super().__init__(parent, title='Set LLRF Tables',centralWidget=widget)
        self.setToolTip('***')
        self.resize(1400,1200)


""" MAIN APPLICATION ENTRY"""
if __name__ == '__main__':
    #Deadly important configuration!
    qapp = QtWidgets.QApplication(sys.argv)
    w=myMainWindow()
    w.setStyleSheet('font-size: 18px;')
    w.show()
    qapp.exec_()
